package com.orchowskia.stenografisto.management.core.services.update;

import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.ValidationError;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;

import java.util.Set;

public class ArticleDeleted extends SystemResponse {


    public ArticleDeleted() {

    }

    public ArticleDeleted(final Fault fault) {
        super(fault);
    }

    public ArticleDeleted(final Fault fault, final Set<ValidationError> validationErrors) {
        super(fault, validationErrors);
    }
}
