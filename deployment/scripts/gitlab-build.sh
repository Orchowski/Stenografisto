#!/bin/bash

#reads actual version from git tags
APP_VERSION=`git describe --tags --abbrev=0`


echo "The version = $APP_VERSION"
echo "And tags:"
git describe --tags

#sets application versions
sed -i -- 's/{APP_VERSION}/'"$APP_VERSION"'/g' Dockerfile
sed -i -- 's/{APP_VERSION}/'"$APP_VERSION"'/g' build.gradle
sed -i -- 's/{APP_VERSION}/'"$APP_VERSION"'/g' ./src/main/resources/application.properties

#build package
./gradlew bootJar

#creating artifacts
mv ./build/libs/management-$APP_VERSION.jar  ./artifacts/management-$APP_VERSION.jar
mv ./src/main/resources/application.properties ./artifacts/application.properties
mv Dockerfile ./artifacts/Dockerfile
echo $APP_VERSION > ./artifacts/APP_VERSION.info

