package com.orchowskia.stenografisto.management.api.messages.responses;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class SimpleFault implements Fault {
    @JsonSerialize
    Faults fault;
    @JsonSerialize
    String message;

    public SimpleFault(final Faults fault, final String message, final String details) {
        this.fault = fault;
        this.message = message;
        this.details = details;
    }

    public SimpleFault(final Faults fault, final String message) {

        this.fault = fault;
        this.message = message;
    }

    public SimpleFault(final Faults fault) {

        this.fault = fault;
    }

    String details;

    @Override
    public Faults type() {
        return fault;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public String details() {
        return details;
    }
}
