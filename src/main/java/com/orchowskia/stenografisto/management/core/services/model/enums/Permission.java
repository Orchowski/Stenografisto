package com.orchowskia.stenografisto.management.core.services.model.enums;

public enum Permission {
    DENIED, READ, READ_WRITE, ADMIN
}
