package com.orchowskia.stenografisto.management.core.services.model.articles;

import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Optional;

public class DetailedArticle {
    public String publicId;
    public String title;
    public String text;
    public Optional<String> author;
    public Permission permission;
    public boolean isPublic;
    public ZonedDateTime created;
    public ZonedDateTime updated;
}
