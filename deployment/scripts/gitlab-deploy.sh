#!/bin/bash
APP_VERSION=`cat ./artifacts/APP_VERSION.info`
echo "app version : $APP_VERSION"
echo "$GOOGLE_KEY" > key

#login to gcloud
gcloud auth activate-service-account --key-file=key

#set version
sed -i -- 's/{APP_VERSION}/'"$APP_VERSION"'/g' ./deployment/kubernetes/deployment.yml

#login to kubectl
mkdir ~/.kube
echo "$KUBECTL_CONF" | base64 -d > ~/.kube/config

#make deploy
kubectl apply -f ./deployment/kubernetes/deployment.yml
kubectl apply -f ./deployment/kubernetes/service.yml