package com.orchowskia.stenografisto.management.storage.repositories;

import com.orchowskia.stenografisto.management.storage.entities.User;
import org.springframework.data.repository.Repository;

import java.util.Optional;


public interface UsersRepository extends Repository<User, String> {

    Optional<User> findUserByPublicId(String publicId);

    User save(User user);
}
