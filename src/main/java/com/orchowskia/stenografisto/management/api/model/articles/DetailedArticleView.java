package com.orchowskia.stenografisto.management.api.model.articles;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.orchowskia.stenografisto.management.api.json.transformers.ZonedZuluDataTimeSerializer;
import com.orchowskia.stenografisto.management.api.model.enums.PermissionView;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class DetailedArticleView {
    public String articleId;
    public String title;
    public String author;
    public String text;
    public boolean isPublic;
    public PermissionView permission;

    @JsonSerialize(using= ZonedZuluDataTimeSerializer.class)
    public ZonedDateTime created;

    @JsonSerialize(using= ZonedZuluDataTimeSerializer.class)
    public ZonedDateTime updated;
}
