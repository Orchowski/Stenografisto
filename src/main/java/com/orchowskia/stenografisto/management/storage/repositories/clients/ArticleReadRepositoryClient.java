package com.orchowskia.stenografisto.management.storage.repositories.clients;

import com.orchowskia.stenografisto.management.core.services.data.ArticlesSource;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;
import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;
import com.orchowskia.stenografisto.management.storage.entities.ArticleOwner;
import com.orchowskia.stenografisto.management.storage.entities.PermissionEntity;
import com.orchowskia.stenografisto.management.storage.mapper.ArticlesStoreMapper;
import com.orchowskia.stenografisto.management.storage.repositories.ArticleOwnersRepository;
import com.orchowskia.stenografisto.management.storage.repositories.ArticlesReadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
class ArticleReadRepositoryClient implements ArticlesSource {
    private final ArticlesReadRepository articlesReadRepository;
    private final ArticleOwnersRepository articleOwnersRepository;
    private final ArticlesStoreMapper mapper;

    @Autowired
    ArticleReadRepositoryClient(final ArticlesReadRepository articlesReadRepository,
                                final ArticleOwnersRepository articleOwnersRepository,
                                final ArticlesStoreMapper mapper) {
        this.articlesReadRepository = articlesReadRepository;
        this.articleOwnersRepository = articleOwnersRepository;
        this.mapper = mapper;
    }

    @Override
    public List<ListedArticle> allArticles() {
        return articlesReadRepository.findAll()
                .stream()
                .map(mapper::toArticle)
                .collect(Collectors.toList());
    }

    @Override
    public List<ListedArticle> userArticles(final String userPublicId) {
        Set<ListedArticle> userArticles = articleOwnersRepository.findArticleOwnersByOwner_PublicId(userPublicId)
                .stream()
                .map(mapper::toArticle)
                .collect(Collectors.toSet());
        List<ListedArticle> publicArticles = articlesReadRepository
                .findArticleByIsPublicTrue().stream()
                .map(mapper::toArticle)
                .collect(Collectors.toList());
        userArticles.addAll(publicArticles);
        return userArticles
                .stream()
                .sorted(Comparator.comparing(x -> x.created))
                .collect(Collectors.toList());
    }

    @Override
    public DetailedArticle article(final String userPublicId, final String articlePublicId) {
        final Optional<ArticleOwner> articleOwnerResponse = articleOwnersRepository.findArticleOwnerByArticle_PublicId(articlePublicId);
        if (articleOwnerResponse.isPresent()) {
            ArticleOwner articleOwner = articleOwnerResponse.get();
            return mapper.toDetailedArticle(articleOwner.getArticle(), articleOwner.getOwnerId().equals(userPublicId));
        }
        return null;
    }

    @Override
    public boolean articleExists(final String userPublicId, final String articlePublicId) {
        final Optional<ArticleOwner> articleOwner = articleOwnersRepository.findArticleOwnersByOwner_PublicId_AndArticle_PublicId(userPublicId, articlePublicId);
        return articleOwner.isPresent();
    }

}
