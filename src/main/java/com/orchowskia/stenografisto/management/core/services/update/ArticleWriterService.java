package com.orchowskia.stenografisto.management.core.services.update;


import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.Faults;
import com.orchowskia.stenografisto.management.api.messages.responses.SimpleFault;
import com.orchowskia.stenografisto.management.api.middleware.UserContext;
import com.orchowskia.stenografisto.management.core.services.data.ArticlesSource;
import com.orchowskia.stenografisto.management.core.services.data.ArticlesStore;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Service
public class ArticleWriterService {
    @Autowired
    private UserContext context;
    private final ArticlesStore articlesStore;
    private final ArticlesSource articlesSource;
    private final Clock clock;

    @Autowired
    public ArticleWriterService(final Clock clock,
                                final ArticlesStore articlesStore,
                                final ArticlesSource articlesSource) {
        this.articlesStore = articlesStore;
        this.articlesSource = articlesSource;
        this.clock = clock;
    }

    public ArticleCreated createArticle(final CreateArticle createArticle) {
        createArticle.creatorPermission = Permission.ADMIN;
        createArticle.brief = resolveBrief(createArticle.text);
        createArticle.created = ZonedDateTime.now(clock);
        final DetailedArticle article = articlesStore.storeNewArticle(createArticle);
        return new ArticleCreated(article.publicId);
    }

    public ArticleUpdated updateArticle(final UpdateArticle updateArticle) {
        if (!articlesSource.articleExists(updateArticle.user, updateArticle.id)) {
            final Fault fault = new SimpleFault(Faults.NO_DATA,
                    String.format("Article '%s' for user '%s\' can not be found", updateArticle.user, updateArticle.id));
            return new ArticleUpdated(fault);
        }
        updateArticle.brief = resolveBrief(updateArticle.text);
        updateArticle.updated = ZonedDateTime.now(clock);

        articlesStore.update(updateArticle);

        return new ArticleUpdated();
    }

    private String resolveBrief(String text) {
        text = Jsoup.parse(text).text();
        int spaceIndex = text.indexOf(" ", 200);
        if (spaceIndex == -1) {
            return text;
        }
        return text.substring(0, spaceIndex);
    }

    public ArticleDeleted delete(String articleId, String userId) {
        ArticleDeleted response = new ArticleDeleted();
        DetailedArticle article = articlesSource.article(userId, articleId);
        if (article == null || !article.author.orElse("").equals(context.getName())) {
            response = new ArticleDeleted(new SimpleFault(Faults.NO_DATA));
        } else {
            articlesStore.delete(articleId);
        }
        return response;
    }
}