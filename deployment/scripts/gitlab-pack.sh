#!/bin/sh
rm Dockerfile
mv ./artifacts/Dockerfile Dockerfile
APP_VERSION=`cat ./artifacts/APP_VERSION.info`
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker build -t  $IMAGE_TAG:$APP_VERSION .
docker tag $IMAGE_TAG:$APP_VERSION $IMAGE_TAG:latest
docker push $IMAGE_TAG
