package com.orchowskia.stenografisto.management.storage.repositories;

import com.orchowskia.stenografisto.management.storage.entities.Article;
import org.springframework.data.repository.Repository;

public interface ArticleWriteRepository extends Repository<Article, Long> {
    Article save(Article newArticle);

}
