package com.orchowskia.stenografisto.management.api.middleware;

import com.orchowskia.stenografisto.management.clients.AuthVerifyRequest;
import com.orchowskia.stenografisto.management.clients.AuthVerifyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Provider;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

public class AuthInterceptor implements HandlerInterceptor {
    private static Logger LOG = LoggerFactory.getLogger(AuthInterceptor.class);

    @Autowired
    private UserContext context;
    private static int a = 0;
    @Value("${stenografisto.users.verify-token}")
    private String authVerifyUrl;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        String authToken = request.getHeader("Authorization");
        LOG.info("auth header : " + authToken);
        LOG.info("URI : " + request.getRequestURI());
        LOG.info("method : " + request.getMethod());
        if (handler instanceof HandlerMethod) {
            if (!verifyAuthorization(authToken) && ((HandlerMethod) handler).hasMethodAnnotation(AuthRequired.class)) {
                LOG.warn("Unauthorized");
                response.sendError(403, "unauthorized operation");
            }
        }
        return true;
    }

    private boolean verifyAuthorization(String authToken) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<AuthVerifyResponse> restResponse
                    = restTemplate.postForEntity(authVerifyUrl, new AuthVerifyRequest(authToken), AuthVerifyResponse.class);
            AuthVerifyResponse responseBody = restResponse.getBody();
            context.setUserId(responseBody.getUserId());
            context.setName(responseBody.getEmail());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
