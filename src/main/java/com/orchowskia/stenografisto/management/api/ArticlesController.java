package com.orchowskia.stenografisto.management.api;

import com.orchowskia.stenografisto.management.api.mapper.ArticlesMapper;
import com.orchowskia.stenografisto.management.api.messages.requests.CreateArticleRequest;
import com.orchowskia.stenografisto.management.api.messages.requests.UpdateArticleRequest;
import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.StenografistoResponse;
import com.orchowskia.stenografisto.management.api.messages.responses.articles.*;
import com.orchowskia.stenografisto.management.api.middleware.AuthRequired;
import com.orchowskia.stenografisto.management.api.middleware.UserContext;
import com.orchowskia.stenografisto.management.api.model.articles.DetailedArticleView;
import com.orchowskia.stenografisto.management.core.services.access.articlereader.ArticleReaderService;
import com.orchowskia.stenografisto.management.core.services.access.articlereader.UserArticles;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.update.ArticleCreated;
import com.orchowskia.stenografisto.management.core.services.update.ArticleDeleted;
import com.orchowskia.stenografisto.management.core.services.update.ArticleUpdated;
import com.orchowskia.stenografisto.management.core.services.update.ArticleWriterService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.function.Function;

@RestController
@RequestMapping("/articles")
public class ArticlesController {
    @Autowired
    private UserContext context;
    private final ArticlesMapper mapper;
    private final ArticleReaderService articleReaderService;
    private final ArticleWriterService articleWriterService;


    @Autowired
    public ArticlesController(final ArticleReaderService articleReaderService,
                              final ArticleWriterService articleWriterService,
                              final ArticlesMapper mapper) {
        this.articleReaderService = articleReaderService;
        this.articleWriterService = articleWriterService;
        this.mapper = mapper;
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public ResponseEntity userArticles() {
        final String userId = ObjectUtils.defaultIfNull(context.getUserId(), "defaultUser");
        final UserArticles userArticlesResponse = articleReaderService.userArticles(userId);
        final ArticlesResponse response = mapper.toResponse(userArticlesResponse);
        return new StenografistoResponse<ArticlesResponse>()
                .withData(response)
                .result();
    }

    @RequestMapping(value = "{articleId}", method = RequestMethod.GET)
    public ResponseEntity userArticles(@PathVariable final String articleId) {
        final String userId = ObjectUtils.defaultIfNull(context.getUserId(), "defaultUser");
        final DetailedArticle detailedArticle = articleReaderService.getUserArticle(userId, articleId);
        final DetailedArticleView detailedArticleView = mapper.toDetailedArticleView(detailedArticle);
        return new StenografistoResponse<ArticleDetailResponse>()
                .withData(new ArticleDetailResponse(detailedArticleView))
                .withStatus(detailedArticleView == null ? HttpStatus.NOT_FOUND : HttpStatus.OK)
                .result();
    }

    @AuthRequired
    @RequestMapping(value = "{articleId}", method = RequestMethod.PATCH)
    public ResponseEntity updateArticle(
            //TODO: different Users from header handling @PathVariable final String userId,
            @PathVariable final String articleId,
            @RequestBody final UpdateArticleRequest requestBody) {
        final String userId = ObjectUtils.defaultIfNull(context.getUserId(), "defaultUser");
        final ArticleUpdated articleUpdated = articleWriterService
                .updateArticle(mapper.toCoreArticleUpdate(requestBody, articleId, userId));
        final ArticleUpdatedResponse response = mapper.toResponse(articleUpdated);

        return new StenografistoResponse<ArticleUpdatedResponse>()
                .withData(response)
                .result();
    }


    @AuthRequired
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity addArticle(
            @RequestBody final CreateArticleRequest requestBody) {
        final String userId = context.getUserId();
        final ArticleCreated articleCreated = articleWriterService
                .createArticle(mapper.toCoreArticleCreate(requestBody, userId));
        final ArticleCreatedResponse response = mapper.toResponse(articleCreated);
        return new StenografistoResponse<ArticleCreatedResponse>()
                .withData(response)
                .result();
    }

    @AuthRequired
    @RequestMapping(value = "{articleId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteArticle(
            @PathVariable final String articleId) {
        final String userId = context.getUserId();
        ArticleDeleted sysResponse = articleWriterService.delete(articleId, userId);
        final ArticleDeletedResponse response = new ArticleDeletedResponse();
        response.copyErrors(sysResponse);
        return new StenografistoResponse<ArticleDeletedResponse>()
                .withData(response)
                .withStatus(response.getFault().map(Fault::type).isPresent() ? HttpStatus.NOT_FOUND : HttpStatus.NO_CONTENT)
                .result();
    }
}
