package com.orchowskia.stenografisto.management.clients;

public class AuthVerifyRequest {
    private String token;

    public AuthVerifyRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
