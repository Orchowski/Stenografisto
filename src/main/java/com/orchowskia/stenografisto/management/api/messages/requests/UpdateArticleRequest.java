package com.orchowskia.stenografisto.management.api.messages.requests;

public class UpdateArticleRequest {
    public String title;
    public boolean isPublic;
    public String text;
}
