package com.orchowskia.stenografisto.management.api.model.enums;

import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;

public enum PermissionView {
    DENIED(Permission.DENIED), READ(Permission.READ), READ_WRITE(Permission.READ_WRITE), ADMIN(Permission.ADMIN);

    Permission modelPermission;

    PermissionView(final Permission modelPermission) {
        this.modelPermission = modelPermission;
    }

    public Permission toModelPermission() {
        return modelPermission;
    }

    public Permission getModelPermission() {
        return modelPermission;
    }
}
