package com.orchowskia.stenografisto.management.api.middleware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Config implements WebMvcConfigurer {
    private static Logger LOG = LoggerFactory.getLogger(UserContext.class);

    @Bean
    public AuthInterceptor authenticationInterceptor() {
        LOG.info("interceptor init");
        return new AuthInterceptor();
    }


    @Bean
    @RequestScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UserContext context(){
        return  new UserContext();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor());
    }
}