package com.orchowskia.stenografisto.management.core.services.data;

import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;

import java.util.List;

public interface ArticlesSource {
    List<ListedArticle> allArticles();

    List<ListedArticle> userArticles(String userPublicId);

    DetailedArticle article(String userPublicId, String articlePublicId);

    boolean articleExists(String userPublicId, String articlePublicId);
}