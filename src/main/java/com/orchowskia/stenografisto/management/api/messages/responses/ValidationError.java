package com.orchowskia.stenografisto.management.api.messages.responses;

public interface ValidationError {
    String field();

    String detail();
}
