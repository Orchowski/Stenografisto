package com.orchowskia.stenografisto.management.core.services.access.articlereader;

import com.orchowskia.stenografisto.management.api.messages.responses.*;
import com.orchowskia.stenografisto.management.core.services.data.ArticlesSource;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ArticleReaderService {

    private final ArticlesSource articlesSource;

    @Autowired
    public ArticleReaderService(final ArticlesSource articlesSource) {
        this.articlesSource = articlesSource;
    }

    public final UserArticles userArticles(final String userId) {
        final Optional<SystemResponse> userIdError = userIdError(userId);
        if (userIdError.isPresent()) {
            return (UserArticles) new UserArticles().copyErrors(userIdError.get());
        }

        final List<ListedArticle> articles = articlesSource.userArticles(userId);
        final UserArticles response = new UserArticles(articles);
        return response;
    }

    public DetailedArticle getUserArticle(final String userId, final String articleId) {
        return articlesSource.article(userId, articleId);
    }

    private Optional<SystemResponse> userIdError(final String userId) {
        if (userId.length() < 3) {
            final Fault fault = new SimpleFault(Faults.VALIDATION, "user cannot be shorter than 3");
            final ValidationError validationError = new SimpleValidationError("userId", "has wrong value");
            final Set<ValidationError> validationErrors = new HashSet<>();
            validationErrors.add(validationError);
            return Optional.of(new UserArticles(fault, validationErrors));
        }
        return Optional.empty();
    }
}
