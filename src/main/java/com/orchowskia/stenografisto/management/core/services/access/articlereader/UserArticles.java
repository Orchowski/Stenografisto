package com.orchowskia.stenografisto.management.core.services.access.articlereader;

import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.ValidationError;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;

import java.util.List;
import java.util.Set;

public class UserArticles extends SystemResponse {

    private List<ListedArticle> articles;

    public List<ListedArticle> getArticles() {
        return articles;
    }

    public UserArticles() {
    }

    public UserArticles(final List<ListedArticle> articleList) {
        articles = articleList;
    }

    public UserArticles(final Fault fault) {
        super(fault);
    }

    public UserArticles(final Fault fault, final Set<ValidationError> validationErrors) {
        super(fault, validationErrors);
    }
}
