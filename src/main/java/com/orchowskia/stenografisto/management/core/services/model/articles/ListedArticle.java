package com.orchowskia.stenografisto.management.core.services.model.articles;

import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Optional;

public class ListedArticle {
    public String publicId;
    public String title;
    public Optional<String> author;
    public Permission permission;
    public String brief;
    public ZonedDateTime created;
    public ZonedDateTime updated;
    public boolean isPublic;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ListedArticle that = (ListedArticle) o;

        return new EqualsBuilder()
                .append(publicId, that.publicId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(publicId)
                .toHashCode();
    }
}
