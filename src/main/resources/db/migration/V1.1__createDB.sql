--ARTICLES
CREATE TABLE article(
article_id serial,
public_id varchar(38) NOT NULL,
title varchar(30) NOT NULL,
"user" varchar(100),
public BOOLEAN DEFAULT false,
"text" text,
brief varchar(240),
created timestamptz NOT NULL,
updated timestamptz NULL,
CONSTRAINT PK_Article PRIMARY KEY (article_id));

--CATEGORIES
CREATE TABLE category(
category_id serial,
name varchar(30) NOT NULL UNIQUE,
description varchar(100),
CONSTRAINT PK_Category PRIMARY KEY (category_id));

--USERS
CREATE TABLE "user"(
public_id varchar(38) NOT NULL UNIQUE,
name varchar(38) NOT NULL,
about text,
CONSTRAINT PK_User PRIMARY KEY (public_id));

--PERMISSIONS-ENUM
CREATE TABLE permission(
id serial,
name VARCHAR(10) NOT NULL UNIQUE,
CONSTRAINT PK_Permission PRIMARY KEY (id));

INSERT INTO permission (name)
VALUES ('DENIED'), ('READ'), ('READ_WRITE'),('ADMIN');

--ARTICLE-USERS
CREATE TABLE article_owner(
article_id INT NOT NULL,
owner_id varchar(38) NOT NULL,
permission int NOT NULL,
CONSTRAINT FK_OwnerArticle_Owner FOREIGN KEY (owner_id) REFERENCES "user"(public_id),
CONSTRAINT FK_OwnerArticle_Permission FOREIGN KEY (permission) REFERENCES permission(id),
CONSTRAINT FK_OwnerArticle_Article FOREIGN KEY (article_id) REFERENCES article(article_id),
CONSTRAINT PK_OwnerArticle PRIMARY KEY (article_id, owner_id));

--ARTICLE-CATEGORIES
CREATE TABLE article_category(
article_id INT NOT NULL,
category_id int NOT NULL,
CONSTRAINT FK_ArticleCategory_Article FOREIGN KEY (article_id) REFERENCES article(article_id),
CONSTRAINT FK_ArticleCategory_Category FOREIGN KEY (category_id) REFERENCES category(category_id),
CONSTRAINT PK_ArticleCategory PRIMARY KEY (article_id, category_id));

