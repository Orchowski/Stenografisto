package com.orchowskia.stenografisto.management.storage.repositories;

import com.orchowskia.stenografisto.management.storage.entities.PermissionEntity;
import org.springframework.data.repository.Repository;


public interface PermissionsRepository extends Repository<PermissionEntity, Integer> {

    PermissionEntity findPermissionByName(String publicId);
}
