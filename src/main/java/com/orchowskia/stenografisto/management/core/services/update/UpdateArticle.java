package com.orchowskia.stenografisto.management.core.services.update;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class UpdateArticle {
    public String id;
    public String title;
    public String user;
    public boolean isPublic;
    public String text;
    public String brief;
    public ZonedDateTime updated;
}
