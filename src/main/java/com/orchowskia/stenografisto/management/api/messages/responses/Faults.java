package com.orchowskia.stenografisto.management.api.messages.responses;

public enum Faults {
    VALIDATION, ACCESS_DENIED, NO_DATA
}
