package com.orchowskia.stenografisto.management.storage.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(ArticleOwnerId.class)
@Table(name = "article_owner")
public class ArticleOwner {

    @NotNull
    @Column(name = "article_id")
    private long articleId;

    @NotNull
    @Column(name = "owner_id")
    private String ownerId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission")
    private PermissionEntity permission;

    @Id
    @ManyToOne
    @JoinColumn(name = "article_id", insertable = false, updatable = false)
    private Article article;

    @Id
    @ManyToOne
    @JoinColumn(name = "owner_id", insertable = false, updatable = false)
    private User owner;


    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(final long articleId) {
        this.articleId = articleId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(final String ownerId) {
        this.ownerId = ownerId;
    }

    public PermissionEntity getPermission() {
        return permission;
    }

    public void setPermission(final PermissionEntity permission) {
        this.permission = permission;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(final Article article) {
        this.article = article;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }
}
