package com.orchowskia.stenografisto.management.api.mapper;

import com.orchowskia.stenografisto.management.api.messages.requests.CreateArticleRequest;
import com.orchowskia.stenografisto.management.api.messages.requests.UpdateArticleRequest;
import com.orchowskia.stenografisto.management.api.messages.responses.articles.ArticleCreatedResponse;
import com.orchowskia.stenografisto.management.api.messages.responses.articles.ArticleUpdatedResponse;
import com.orchowskia.stenografisto.management.api.messages.responses.articles.ArticlesResponse;
import com.orchowskia.stenografisto.management.api.model.articles.DetailedArticleView;
import com.orchowskia.stenografisto.management.api.model.articles.ListedArticleView;
import com.orchowskia.stenografisto.management.api.model.enums.PermissionView;
import com.orchowskia.stenografisto.management.core.services.access.articlereader.UserArticles;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;
import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;
import com.orchowskia.stenografisto.management.core.services.update.ArticleCreated;
import com.orchowskia.stenografisto.management.core.services.update.ArticleUpdated;
import com.orchowskia.stenografisto.management.core.services.update.CreateArticle;
import com.orchowskia.stenografisto.management.core.services.update.UpdateArticle;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ArticlesMapper {
    public ArticlesResponse toResponse(final UserArticles userArticles) {
        if (userArticles.isFaulted()) {
            return (ArticlesResponse) new ArticlesResponse().copyErrors(userArticles);
        }
        final List<ListedArticleView> articles = userArticles.getArticles()
                .stream()
                .map(this::toListedArticleView).collect(Collectors.toList());
        return new ArticlesResponse(articles);
    }

    public CreateArticle toCoreArticleCreate(final CreateArticleRequest articleRequest, final String userId) {
        final CreateArticle createArticle = new CreateArticle();
        createArticle.user = userId;
        createArticle.isPublic = articleRequest.isPublic;
        createArticle.text = articleRequest.text;
        createArticle.title = articleRequest.title;
        return createArticle;
    }

    public ArticleCreatedResponse toResponse(final ArticleCreated articleCreated) {
        if (articleCreated.isFaulted()) {
            return (ArticleCreatedResponse) new ArticleCreatedResponse().copyErrors(articleCreated);
        }
        return new ArticleCreatedResponse(articleCreated.articleId);
    }

    private ListedArticleView toListedArticleView(final ListedArticle article) {
        final ListedArticleView articleView = new ListedArticleView();
        articleView.articleId = article.publicId;
        articleView.author = article.author.orElse(StringUtils.EMPTY); //karolinoak100@gmail.com, comodsuda@gmail.com
        articleView.permission = toPermission(article.permission);
        articleView.title = article.title;
        articleView.brief = article.brief;
        articleView.created = article.created;
        return articleView;
    }

    private PermissionView toPermission(final Permission permission) {
        return Arrays.stream(PermissionView.values())
                .filter(value -> value.getModelPermission().equals(permission))
                .findFirst()
                .orElse(PermissionView.READ);
    }

    public DetailedArticleView toDetailedArticleView(final DetailedArticle detailedArticle) {
        if (detailedArticle == null) {
            return null;
        }
        final DetailedArticleView articleView = new DetailedArticleView();
        articleView.articleId = detailedArticle.publicId;
        articleView.author = detailedArticle.author.orElse(StringUtils.EMPTY);
        articleView.permission = toPermission(detailedArticle.permission);
        articleView.title = detailedArticle.title;
        articleView.text = detailedArticle.text;
        articleView.isPublic = detailedArticle.isPublic;
        articleView.created = detailedArticle.created;
        articleView.updated = detailedArticle.updated;
        return articleView;
    }

    public UpdateArticle toCoreArticleUpdate(final UpdateArticleRequest requestBody, final String id, final String userId) {
        final UpdateArticle updateArticle = new UpdateArticle();
        updateArticle.id = id;
        updateArticle.user = userId;
        updateArticle.isPublic = requestBody.isPublic;
        updateArticle.text = requestBody.text;
        updateArticle.title = requestBody.title;
        return updateArticle;
    }

    public ArticleUpdatedResponse toResponse(final ArticleUpdated articleUpdated) {
        if (articleUpdated.isFaulted()) {
            return (ArticleUpdatedResponse) new ArticleUpdatedResponse().copyErrors(articleUpdated);
        }
        return new ArticleUpdatedResponse();
    }
}