package com.orchowskia.stenografisto.management.storage.entities;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "article_id", columnDefinition = "serial", updatable = false, nullable = false)
    private long id;

    @Column(name = "public_id")
    private String publicId;

    @Column(name = "title", length = 30)
    private String title;

    @Column(name = "\"user\"", length = 100)
    private String author;

    @Column(name = "public")
    private boolean isPublic;

    @Column(name = "text")
    private String text;

    @Column(name = "brief")
    private String brief;

    @Column(name = "created")
    private ZonedDateTime created;

    @Column(name = "updated")
    private ZonedDateTime updated;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "article_owner",
            joinColumns = {@JoinColumn(name = "article_id")},
            inverseJoinColumns = {@JoinColumn(name = "owner_id")})
    private Set<User> users = new HashSet<>();

    public Article() {
    }

    public boolean isPublic() {
        return isPublic;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(final String publicId) {
        this.publicId = publicId;
    }

    public void setPublic(final boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Optional<User> getAuthorDetails(){
        return users.stream().filter(x->x.getPublicId().equals(author)).findFirst();
    }

    public void setUsers(final Set<User> users) {
        this.users = users;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public ZonedDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }
}
