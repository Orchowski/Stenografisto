FROM java:8-jdk-alpine
WORKDIR /opt/app
MAINTAINER Aleksander Orchowski - www.orchowskia.com

COPY ./artifacts/management-{APP_VERSION}.jar ./

ENV SPRING_PROFILE="default"

CMD java -Djava.security.egd=file:/dev/./urandom \
 -Dspring.profiles.active=$SPRING_PROFILE \
 -jar management-{APP_VERSION}.jar
