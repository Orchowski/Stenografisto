package com.orchowskia.stenografisto.management.storage.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ArticleOwnerId implements Serializable {

    @Column(name = "article_id")
    private long articleId;

    @Column(name = "owner_id")
    private String ownerId;

    public long getArticleId() {
        return articleId;
    }

    public String getOwnerId() {
        return ownerId;
    }
}
