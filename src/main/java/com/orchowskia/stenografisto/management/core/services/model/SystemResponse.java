package com.orchowskia.stenografisto.management.core.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.ValidationError;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@JsonInclude(Include.NON_EMPTY)
public abstract class SystemResponse {
    Optional<Fault> fault = Optional.ofNullable(null);

    Optional<Set<ValidationError>> validationErrors = Optional.ofNullable(null);

    public SystemResponse() {
    }

    public SystemResponse(final Fault fault) {
        this.fault = Optional.ofNullable(fault);
    }

    public SystemResponse(final Fault fault, final Set<ValidationError> validationErrors) {
        this.fault = Optional.ofNullable(fault);
        this.validationErrors = Optional.ofNullable(validationErrors);
    }

    @JsonIgnore
    public boolean isFaulted() {
        return fault.isPresent();
    }

    public Optional<Fault> getFault() {
        return fault;
    }

    public Optional<Set<ValidationError>> getValidationErrors() {
        return validationErrors;
    }

    public SystemResponse copyErrors(final SystemResponse anotherOne) {
        fault = anotherOne.fault;
        validationErrors.ifPresent(err -> err.addAll(anotherOne.validationErrors.orElse(new HashSet<>())));
        validationErrors = anotherOne.validationErrors.isPresent() ? anotherOne.validationErrors : validationErrors;
        return this;
    }
}
