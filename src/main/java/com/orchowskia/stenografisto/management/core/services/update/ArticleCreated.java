package com.orchowskia.stenografisto.management.core.services.update;

import com.orchowskia.stenografisto.management.api.messages.responses.Fault;
import com.orchowskia.stenografisto.management.api.messages.responses.ValidationError;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;

import java.util.Set;

public class ArticleCreated extends SystemResponse {
    public String articleId;

    public ArticleCreated(final String articleId) {
        this.articleId = articleId;
    }

    public ArticleCreated(final Fault fault) {
        super(fault);
    }

    public ArticleCreated(final Fault fault, final Set<ValidationError> validationErrors) {
        super(fault, validationErrors);
    }
}
