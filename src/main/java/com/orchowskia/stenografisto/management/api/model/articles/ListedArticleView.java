package com.orchowskia.stenografisto.management.api.model.articles;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.orchowskia.stenografisto.management.api.json.transformers.ZonedZuluDataTimeSerializer;
import com.orchowskia.stenografisto.management.api.model.enums.PermissionView;

import java.time.ZonedDateTime;

public class ListedArticleView {
    public String articleId;
    public String title;
    public String brief;
    public String author;
    public PermissionView permission;

    @JsonSerialize(using= ZonedZuluDataTimeSerializer.class)
    public ZonedDateTime created;
}