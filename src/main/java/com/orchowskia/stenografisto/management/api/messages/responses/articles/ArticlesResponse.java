package com.orchowskia.stenografisto.management.api.messages.responses.articles;

import com.orchowskia.stenografisto.management.api.model.articles.ListedArticleView;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;

import java.util.List;


public class ArticlesResponse extends SystemResponse {

    private List<ListedArticleView> articles;

    public ArticlesResponse() {
    }

    public ArticlesResponse(final List<ListedArticleView> articles) {
        this.articles = articles;
    }

    public List<ListedArticleView> getArticles() {
        return articles;
    }
}

