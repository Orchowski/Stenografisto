package com.orchowskia.stenografisto.management.api.messages.responses;

import com.orchowskia.stenografisto.management.core.services.model.EmptySystemResponse;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public class HttpStatusFactory {
    SystemResponse response;
    HttpStatus status;
    private boolean shouldUseCustom;

    public HttpStatusFactory() {
    }

    public HttpStatusFactory(final SystemResponse response) {
        this.response = response;
    }

    public void useCustomStatus(final HttpStatus status) {
        shouldUseCustom = true;
        this.status = status;
    }

    public void withResponse(final SystemResponse response) {
        if (response instanceof EmptySystemResponse) {
            withResponse((EmptySystemResponse) response);
            return;
        }
        this.response = response;
    }

    private void withResponse(final EmptySystemResponse response) {
        this.response = null;
    }

    public HttpStatus toHttpStatus() {
        if (shouldUseCustom) {
            return status;
        }
        final Optional<SystemResponse> response = Optional.ofNullable(this.response);

        if (!response.isPresent()) {
            status = HttpStatus.NO_CONTENT;
            return status;
        }

        status = HttpStatus.OK;
        response.map(SystemResponse::getFault).ifPresent((fault) -> {
            if (fault.isPresent()) {
                final Faults faultType = fault.get().type();
                if (faultType.equals(Faults.VALIDATION)) {
                    status = HttpStatus.BAD_REQUEST;
                }
                if (faultType.equals(Faults.ACCESS_DENIED)) {
                    status = HttpStatus.FORBIDDEN;
                }
                if (faultType.equals(Faults.NO_DATA)) {
                    status = HttpStatus.NOT_FOUND;
                }
            }
        });

        return status;
    }
}
