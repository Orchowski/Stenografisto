package com.orchowskia.stenografisto.management.api.messages.responses;

public interface Fault {
    Faults type();

    String message();

    String details();
}
