package com.orchowskia.stenografisto.management.core.services.data;

import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.update.CreateArticle;
import com.orchowskia.stenografisto.management.core.services.update.UpdateArticle;

public interface ArticlesStore {
    DetailedArticle storeNewArticle(CreateArticle createArticle);

    void update(UpdateArticle updateArticle);

    void delete(String id);
}
