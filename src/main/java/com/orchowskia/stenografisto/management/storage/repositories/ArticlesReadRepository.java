package com.orchowskia.stenografisto.management.storage.repositories;

import com.orchowskia.stenografisto.management.storage.entities.Article;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;


public interface ArticlesReadRepository extends Repository<Article, Long> {

    List<Article> findAll();
    List<Article> findArticleByIsPublicTrue();
    Optional<Article> findByPublicId(String publicId);
}
