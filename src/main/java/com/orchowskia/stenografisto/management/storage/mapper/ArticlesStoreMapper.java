package com.orchowskia.stenografisto.management.storage.mapper;

import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.model.articles.ListedArticle;
import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;
import com.orchowskia.stenografisto.management.core.services.update.CreateArticle;
import com.orchowskia.stenografisto.management.storage.entities.Article;
import com.orchowskia.stenografisto.management.storage.entities.ArticleOwner;
import com.orchowskia.stenografisto.management.storage.entities.PermissionEntity;
import com.orchowskia.stenografisto.management.storage.entities.User;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class ArticlesStoreMapper {
    public ListedArticle toArticle(final Article articleEntity) {
        final ListedArticle listedArticle = new ListedArticle();
        listedArticle.author = articleEntity.getAuthorDetails().map(User::getName);
        listedArticle.publicId = articleEntity.getPublicId();
        listedArticle.title = articleEntity.getTitle();
        listedArticle.brief = articleEntity.getBrief();
        listedArticle.isPublic = articleEntity.isPublic();
        listedArticle.created = articleEntity.getCreated();
        listedArticle.updated = articleEntity.getUpdated();
        return listedArticle;
    }

    public DetailedArticle toDetailedArticle(final Article articleEntity, final boolean isOwner) {
        final DetailedArticle detailedArticle = new DetailedArticle();
        detailedArticle.author = articleEntity.getAuthorDetails().map(User::getName);
        detailedArticle.publicId = articleEntity.getPublicId();
        detailedArticle.title = articleEntity.getTitle();
        detailedArticle.text = articleEntity.getText();
        detailedArticle.isPublic = articleEntity.isPublic();
        detailedArticle.permission = toPermission(isOwner);
        detailedArticle.created = articleEntity.getCreated();
        detailedArticle.updated = articleEntity.getUpdated();
        return detailedArticle;
    }

    public ListedArticle toArticle(final ArticleOwner articleOwnerEntity) {
        final ListedArticle listedArticle = toArticle(articleOwnerEntity.getArticle());
        listedArticle.permission = toPermission(articleOwnerEntity.getPermission());
        return listedArticle;
    }

    private Permission toPermission(final boolean isOwner) {
        return isOwner ? Permission.ADMIN : Permission.READ;
    }

    private Permission toPermission(final PermissionEntity permissionEntity) {
        return Permission.valueOf(permissionEntity.getName());
    }

    public Article toArticle(final CreateArticle createArticle) {
        final Article article = new Article();
        article.setTitle(createArticle.title);
        article.setAuthor(createArticle.user);
        article.setPublic(createArticle.isPublic);
        article.setText(createArticle.text);
        article.setBrief(createArticle.brief);
        article.setCreated(createArticle.created);
        article.setPublicId(UUID.randomUUID().toString());
        return article;
    }
}
