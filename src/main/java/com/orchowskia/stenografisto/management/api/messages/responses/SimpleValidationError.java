package com.orchowskia.stenografisto.management.api.messages.responses;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class SimpleValidationError implements ValidationError {
    @JsonSerialize
    String field;

    @JsonSerialize
    String detail;

    public SimpleValidationError(final String field, final String detail) {
        this.field = field;
        this.detail = detail;
    }

    @Override
    public String field() {
        return field;
    }

    @Override
    public String detail() {
        return detail;
    }
}
