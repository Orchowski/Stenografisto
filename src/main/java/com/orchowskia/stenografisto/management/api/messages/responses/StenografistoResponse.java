package com.orchowskia.stenografisto.management.api.messages.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.Set;

public class StenografistoResponse<T extends SystemResponse> {
    ResponseData<T> content = new ResponseData<>();
    HttpStatusFactory statusFactory = new HttpStatusFactory();

    public StenografistoResponse() {
    }

    public StenografistoResponse(final HttpStatus status) {
        statusFactory.useCustomStatus(status);
    }

    public StenografistoResponse<T> withData(final T data) {
        if (data == null) {
            return this;
        }

        statusFactory.withResponse(data);
        content.data = data;
        content.fault = data.getFault();
        content.validationErrors = data.getValidationErrors();
        return this;
    }

    public StenografistoResponse<T> withStatus(final HttpStatus status) {
        statusFactory.useCustomStatus(status);
        return this;
    }

    public ResponseEntity<ResponseData<T>> result() {
        if (content.data == null || content.data.isFaulted()) {
            content.data = null;
        }
        return new ResponseEntity<>(content, statusFactory.toHttpStatus());
    }

    @JsonInclude(Include.NON_EMPTY)
    class ResponseData<T> {
        T data;
        Optional<Fault> fault;
        Optional<Set<ValidationError>> validationErrors;

        public Optional<Fault> getFault() {
            return fault;
        }

        public Optional<Set<ValidationError>> getValidationErrors() {
            return validationErrors;
        }

        public T getData() {
            return data;
        }
    }
}
