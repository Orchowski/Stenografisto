package com.orchowskia.stenografisto.management.api.messages.responses.articles;

import com.orchowskia.stenografisto.management.api.model.articles.DetailedArticleView;
import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;


public class ArticleDetailResponse extends SystemResponse {

    private DetailedArticleView article;

    public ArticleDetailResponse() {
    }

    public ArticleDetailResponse(final DetailedArticleView article) {
        this.article = article;
    }

    public DetailedArticleView getArticle() {
        return article;
    }
}

