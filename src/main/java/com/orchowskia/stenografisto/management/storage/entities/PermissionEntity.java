package com.orchowskia.stenografisto.management.storage.entities;

import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "permission")
public class PermissionEntity {

    @Id
    @Unique
    @NotNull
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "permission")
    private Set<ArticleOwner> owners;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Set<ArticleOwner> getOwners() {
        return owners;
    }

    public void setOwners(final Set<ArticleOwner> owners) {
        this.owners = owners;
    }
}
