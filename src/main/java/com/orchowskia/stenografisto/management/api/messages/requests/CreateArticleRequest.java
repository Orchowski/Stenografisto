package com.orchowskia.stenografisto.management.api.messages.requests;

public class CreateArticleRequest {
    public String title;
    public boolean isPublic;
    public String text = "";
}
