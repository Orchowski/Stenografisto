package com.orchowskia.stenografisto.management.api.messages.responses.articles;

import com.orchowskia.stenografisto.management.core.services.model.SystemResponse;


public class ArticleCreatedResponse extends SystemResponse {

    private String id;

    public ArticleCreatedResponse() {
    }

    public ArticleCreatedResponse(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

