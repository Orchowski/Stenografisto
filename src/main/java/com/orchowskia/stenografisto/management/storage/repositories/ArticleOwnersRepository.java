package com.orchowskia.stenografisto.management.storage.repositories;

import com.orchowskia.stenografisto.management.storage.entities.ArticleOwner;
import com.orchowskia.stenografisto.management.storage.entities.ArticleOwnerId;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.Set;


public interface ArticleOwnersRepository extends Repository<ArticleOwner, ArticleOwnerId> {

    Set<ArticleOwner> findArticleOwnersByOwner_PublicId(String publicId);

    Optional<ArticleOwner> findArticleOwnerByArticle_PublicId(String publicId);

    Optional<ArticleOwner> findArticleOwnersByOwner_PublicId_AndArticle_PublicId(String publicId, String articleId);

    ArticleOwner save(ArticleOwner articleOwner);
}
