package com.orchowskia.stenografisto.management.storage.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "public_id")
    private String publicId;

    @Column(name = "name")
    private String name;

    @Column(name = "about")
    private String about;

    @ManyToMany(mappedBy = "users")
    private Set<Article> articles;

    @OneToMany
    @JoinTable(name = "article_owner")
    private Set<ArticleOwner> articleOwners;

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(final String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(final String about) {
        this.about = about;
    }

    public Set<Article> getArticles() {
        return articles;
    }

    public void setArticles(final Set<Article> articles) {
        this.articles = articles;
    }

    public Set<ArticleOwner> getArticleOwners() {
        return articleOwners;
    }

    public void setArticleOwners(final Set<ArticleOwner> articleOwners) {
        this.articleOwners = articleOwners;
    }
}
