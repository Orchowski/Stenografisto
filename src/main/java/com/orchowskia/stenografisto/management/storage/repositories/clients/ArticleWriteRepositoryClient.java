package com.orchowskia.stenografisto.management.storage.repositories.clients;

import com.orchowskia.stenografisto.management.api.middleware.UserContext;
import com.orchowskia.stenografisto.management.core.services.data.ArticlesStore;
import com.orchowskia.stenografisto.management.core.services.model.articles.DetailedArticle;
import com.orchowskia.stenografisto.management.core.services.update.CreateArticle;
import com.orchowskia.stenografisto.management.core.services.update.UpdateArticle;
import com.orchowskia.stenografisto.management.storage.entities.Article;
import com.orchowskia.stenografisto.management.storage.entities.ArticleOwner;
import com.orchowskia.stenografisto.management.storage.entities.User;
import com.orchowskia.stenografisto.management.storage.mapper.ArticlesStoreMapper;
import com.orchowskia.stenografisto.management.storage.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class ArticleWriteRepositoryClient implements ArticlesStore {
    @Autowired
    private UserContext context;
    EntityManager entityManager;
    final ArticleWriteRepository articleWriteRepository;
    private final ArticlesStoreMapper storeMapper;
    private final ArticleOwnersRepository articleOwnersRepository;
    private final ArticlesReadRepository articlesReadRepository;
    private final UsersRepository usersRepository;
    private final PermissionsRepository permissionsRepository;

    @Autowired
    public ArticleWriteRepositoryClient(final ArticleWriteRepository articleWriteRepository,
                                        final ArticleOwnersRepository articleOwnersRepository,
                                        final ArticlesReadRepository articlesReadRepository,
                                        final UsersRepository usersRepository,
                                        final PermissionsRepository permissionsRepository,
                                        final ArticlesStoreMapper storeMapper,
                                        final EntityManager entityManager) {
        this.articleWriteRepository = articleWriteRepository;
        this.articleOwnersRepository = articleOwnersRepository;
        this.usersRepository = usersRepository;
        this.storeMapper = storeMapper;
        this.permissionsRepository = permissionsRepository;
        this.entityManager = entityManager;
        this.articlesReadRepository = articlesReadRepository;
    }

    @Override
    @Transactional
    public DetailedArticle storeNewArticle(final CreateArticle createArticle) {
        final Article article = storeMapper.toArticle(createArticle);

        final Optional<User> userOptional = usersRepository.findUserByPublicId(createArticle.user);
        final User user;
        if (!userOptional.isPresent()) {
            user = new User();
            user.setPublicId(createArticle.user);
            user.setName(context.getName());
            usersRepository.save(user);
        } else {
            user = userOptional.get();
        }

        final Article result = articleWriteRepository.save(article);
        final ArticleOwner articleOwner = new ArticleOwner();
        articleOwner.setArticleId(result.getId());
        articleOwner.setOwnerId(user.getPublicId());
        articleOwner.setPermission(permissionsRepository
                .findPermissionByName(createArticle.creatorPermission.name())
        );

        articleOwnersRepository.save(articleOwner);
        return storeMapper.toDetailedArticle(result, true);
    }

    @Override
    @Transactional
    public void update(final UpdateArticle updateArticle) {
        final Article article = articleOwnersRepository.findArticleOwnersByOwner_PublicId_AndArticle_PublicId(updateArticle.user,
                updateArticle.id).get().getArticle();
        article.setTitle(updateArticle.title);
        article.setPublic(updateArticle.isPublic);
        article.setText(updateArticle.text);
        article.setBrief(updateArticle.brief);
        article.setUpdated(updateArticle.updated);
        articleWriteRepository.save(article);
    }

    @Override
    @Transactional
    public void delete(String id) {
        Optional<Article> oa = articlesReadRepository.findByPublicId(id);
        oa.ifPresent(ao -> {
            Query query = entityManager.
                    createQuery("DELETE FROM ArticleOwner art WHERE art.articleId = :id");
            Query query2 = entityManager.
                    createQuery("DELETE FROM Article art WHERE art.id = :id");
            query.setParameter("id", ao.getId());
            query2.setParameter("id", ao.getId());
            query.executeUpdate();
            query2.executeUpdate();
        });
    }
}
