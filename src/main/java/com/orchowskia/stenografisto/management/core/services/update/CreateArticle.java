package com.orchowskia.stenografisto.management.core.services.update;

import com.orchowskia.stenografisto.management.core.services.model.enums.Permission;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class CreateArticle {
    public String title;
    public String user;
    public boolean isPublic;
    public String text;
    public String brief;
    public ZonedDateTime created;
    public Permission creatorPermission;
}
